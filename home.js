/*
	Developer:		Michael Kha
	Software:		MM Text
	Version:		1.0.1
	
	Credit: Original owner of Z1_Uni(input) and Uni_Z1(input) functions
*/
(function () {
	"use strict";

	// The initialize function is run each time the page is loaded.
	Office.initialize = function (reason) {
		$(document).ready(function () {

			// Use this to check whether the API is supported in the Word client.
			if (Office.context.requirements.isSetSupported('WordApi', 1.1)) {
				// Do something that is only available via the new APIs
				$('#zg2uni').click(case1);
				$('#uni2zg').click(case2);
				$('#norzg').click(case3);
				$('#supportedVersion').html('ဤ Plugin ကို Word 2016 နှင့်အထက် ဗားရှင်းများဖြင့်သာ  အသုံးပြုနိုင်ပါသည်။');
			}
			else {
				// Just letting you know that this code will not work with your version of Word.
				$('#supportedVersion').html('ဤ Plugin ကို Word 2016 နှင့်အထက် ဗားရှင်းများဖြင့်သာ  အသုံးပြုနိုင်ပါသည်။');
			}
		});
	};
	
	function case1(){
		initialize(1);
	}
	function case2(){
		initialize(2);
	}
	function case3(){
		initialize(3);
	}

	function initialize(caseid){
		Word.run(function (context)
		{
			Office.context.document.getSelectedDataAsync(Office.CoercionType.Text, 
				function (result) {
					write(result.value);
				}
			);
		});
		if(caseid==1){
			setTimeout(changeZawgyiToUnicode, 1000);
		}else if(caseid==2){
			setTimeout(changeUnicodeToZawgyi, 1000);
		}else if(caseid==3){
			setTimeout(normalizeZawgyi, 1000);
		}
	}

	function changeZawgyiToUnicode()
	{
		Word.run(function (context)
		{
			
			
			var thisDocument = context.document;
			var range = thisDocument.getSelection();
			var myText = document.getElementById('storage').value;
			var myUnicode = Z1_Uni(normalizeMM(flatMM(myText)));
			range.insertText(myUnicode, Word.InsertLocation.replace);
			range.font.name = document.getElementById("unifont").value;
			range.font.size = 11;
			range.font.bold = false;
			
			return context.sync().then(function () {
				console.log('Changed from Zawgyi to Unicode.');
				//document.getElementById('message').innerHTML = 'Changed from Zawgyi to Unicode.';
			});
		})
		.catch(function (error) {
			console.log('Error: ' + JSON.stringify(error));
			if (error instanceof OfficeExtension.Error) {
				console.log('Debug info: ' + JSON.stringify(error.debugInfo));
			}
		});
	}
	
	function changeUnicodeToZawgyi()
	{
		Word.run(function (context)
		{
			Office.context.document.getSelectedDataAsync(Office.CoercionType.Text, 
				function (result) {
					write(result.value);
				}
			);
			
			var thisDocument = context.document;
			var range = thisDocument.getSelection();
			var myText = document.getElementById('storage').value;
			var myZawgyi = normalizeMM(flatMM(Uni_Z1(myText)));
			
			if(document.getElementById('zerowidth').checked==true){
				myZawgyi = zerowidth(myZawgyi);	
			}
			
			range.insertText(myZawgyi, Word.InsertLocation.replace);
			range.font.name = "Zawgyi-one";
			range.font.size = 10;
			range.font.bold = false;
			
			return context.sync().then(function () {
				console.log('Changed from Unicode to Zawgyi.');
				//document.getElementById('message').innerHTML = 'Changed from Unicode to Zawgyi.';
			});
			
		})
		.catch(function (error) {
			console.log('Error: ' + JSON.stringify(error));
			if (error instanceof OfficeExtension.Error) {
				console.log('Debug info: ' + JSON.stringify(error.debugInfo));
			}
		});
	}
	
	function normalizeZawgyi()
	{
		Word.run(function (context)
		{
			Office.context.document.getSelectedDataAsync(Office.CoercionType.Text, 
				function (result) {
					write(result.value);
				}
			);
			
			var thisDocument = context.document;
			var range = thisDocument.getSelection();
			var myText = document.getElementById('storage').value;
			var myZawgyi = normalizeMM(flatMM(myText));
			
			if(document.getElementById('zerowidth').checked==true){
				myZawgyi = zerowidth(myZawgyi);	
			}
			
			range.insertText(myZawgyi, Word.InsertLocation.replace);
			range.font.name = "Zawgyi-one";
			range.font.size = 10;
			range.font.bold = false;
			
			return context.sync().then(function () {
				console.log('Normalized Zawgyi.');
				//document.getElementById('message').innerHTML = 'Normalized Zawgyi.';
			});
			
		})
		.catch(function (error) {
			console.log('Error: ' + JSON.stringify(error));
			if (error instanceof OfficeExtension.Error) {
				console.log('Debug info: ' + JSON.stringify(error.debugInfo));
			}
		});
	}

	function zerowidth(txt){
		//prefix invisible character
		txt = txt.replace(/(\u1031|\u103B|\u107E|\u107F|\u1080|\u1081|\u1083|\u1084|\u108F)/g,'\u200B$1');
		
		//remove prefix space
		txt = txt.replace(/[\u0020]+/g,'\u0020');
		txt = txt.replace(/\u0020([\u102B-\u102E])/g,'$1');
		txt = txt.replace(/\u0020([\u1032-\u1038])/g,'$1');
		txt = txt.replace(/\u0020\u103C/g,'\u103C');
		txt = txt.replace(/\u0020\u104A/g,'\u104A');
		txt = txt.replace(/\u0020\u104B/g,'\u104B');
		txt = txt.replace(/\u0020\u105A/g,'\u105A');
		txt = txt.replace(/\u0020\u1094/g,'\u1094');
		txt = txt.replace(/\u0020\u1095/g,'\u1095');
		txt = txt.replace(/[\u0020]+/g,'\u0020');
		
		//remove surfix space
		txt = txt.replace(/\u030F\u0020/g,'\u030F');
		txt = txt.replace(/\u1037\u200b/g,'\u1037');
		txt = txt.replace(/[\u0020]+/g,'\u0020');
		
		//surfix zerowidth space
		txt = txt.replace(/\u0029/g,'\u0029\u200B');	
		txt = txt.replace(/\u1037/g,'\u1037\u200B');
		txt = txt.replace(/\u1038/g,'\u1038\u200B');
		txt = txt.replace(/\u1039/g,'\u1039\u200B');
		txt = txt.replace(/\u1094/g,'\u1094\u200B');
		txt = txt.replace(/\u1095/g,'\u1095\u200B');
		txt = txt.replace(/[\u2060]+/g,'\u2060');
		txt = txt.replace(/[\u200B]+/g,'\u200B');
		
		//remove refix zerowidth space
		txt = txt.replace(/\u200B\u104A/g,'\u104A');
		txt = txt.replace(/\u200B\u104B/g,'\u104B');
		txt = txt.replace(/\u200B\u104A/g,'\u104A');
		txt = txt.replace(/\u200B\u1095/g,'\u1095');
		txt = txt.replace(/\u200B\u1094/g,'\u1094');
		txt = txt.replace(/\u200B\u105A/g,'\u105A');
		txt = txt.replace(/\u200B\u103C/g,'\u103C');
		txt = txt.replace(/\u200B\u1038/g,'\u1038');
		txt = txt.replace(/\u200B\u1036/g,'\u1036');
		txt = txt.replace(/\u200B\u1034/g,'\u1034');
		txt = txt.replace(/\u200B\u1033/g,'\u1033');
		txt = txt.replace(/\u200B\u1032/g,'\u1032');
		txt = txt.replace(/\u200B\u102E/g,'\u102E');
		txt = txt.replace(/\u200B\u102D/g,'\u102D');
		txt = txt.replace(/\u200B\u102C/g,'\u102C');

		txt = txt.replace(/[\u200B]+/g,'\u200B');
		txt = txt.replace(/[\u2060]+/g,'\u2060');
		txt = txt.replace(/[\u0020]+/g,'\u0020');
		
		txt = txt.replace(/([\u1031\u103B\u107E-\u1084])[\u200B\u0020\u2060]/g,'$1');
		txt = txt.replace(/[\u200B\u0020\u2060\s]([\u1085\u106C\u106D\u105A\u104A\u104B\u102B-\u1030\u1032-\u103A\u1060-\u1069\u1070-\u107D\u1087-\u108E\u1093-\u1095])/g,'$1');
		
		
		return txt;
	}

	function write(message)
	{
		document.getElementById('storage').value = message; 
	}
			
	function Z1_Uni(input)
	{
		
	   var output=input;
	   output = output.replace(/[\u200B]+/g,'');
	   var tallAA = "\u102B";
	   var AA = "\u102C";
	   var vi = "\u102D";
	   var ii = "\u102E";
	   var u = "\u102F";
	   var uu = "\u1030";
	   var ve = "\u1031";
	   var ai = "\u1032";
	   var ans = "\u1036";
	   var db = "\u1037";
	   var visarga = "\u1038";
	   var asat = "\u103A";
	   var ya = "\u103B";
	   var ra = "\u103C";
	   var wa = "\u103D";
	   var ha = "\u103E";
	   var zero = "\u1040";
	   output =  output.replace( /\u106A/g, " \u1009");
	   output =  output.replace( /\u1025(?=[\u1039\u102C])/g, "\u1009"); //new
	   output =  output.replace( /\u1025\u102E/g, "\u1026"); //new
	   output =  output.replace( /\u106B/g, "\u100A");
	   output =  output.replace( /\u1090/g, "\u101B");
	   output =  output.replace( /\u1040/g, zero);
	   output =  output.replace( /\u108F/g, "\u1014");
	   output =  output.replace( /\u1012/g, "\u1012");
	   output =  output.replace( /\u1013/g, "\u1013");
	   output =  output.replace( /[\u103D\u1087]/g, ha);
	   output =  output.replace( /\u103C/g, wa);
	   output =  output.replace( /[\u103B\u107E\u107F\u1080\u1081\u1082\u1083\u1084]/g, ra);
	   output =  output.replace( /[\u103A\u107D]/g, ya);
	   output =  output.replace( /\u103E\u103B/g, ya + ha);
	   output =  output.replace( /\u108A/g, wa + ha);
	   output =  output.replace( /\u103E\u103D/g, wa + ha);
	   output =  output.replace( /(\u1031)?(\u103C)?([\u1000-\u1021])\u1064/g, "\u1064$1$2$3");
	   output =  output.replace( /(\u1031)?(\u103C)?([\u1000-\u1021])\u108B/g, "\u1064$1$2$3\u102D");
	   output =  output.replace( /(\u1031)?(\u103C)?([\u1000-\u1021])\u108C/g, "\u1064$1$2$3\u102E");
	   output =  output.replace( /(\u1031)?(\u103C)?([\u1000-\u1021])\u108D/g, "\u1064$1$2$3\u1036");
	   output =  output.replace( /\u105A/g, tallAA + asat);
	   output =  output.replace( /\u108E/g, vi + ans);
	   output =  output.replace( /\u1033/g, u);
	   output =  output.replace( /\u1034/g, uu);
	   output =  output.replace( /\u1088/g, ha+u);
	   output =  output.replace( /\u1089/g, ha+uu);
	   output =  output.replace( /\u1039/g, "\u103A");
	   output =  output.replace( /[\u1094\u1095]/g, db);
	   output =  output.replace( /([\u1000-\u1021])([\u102C\u102D\u102E\u1032\u1036]){1,2}([\u1060\u1061\u1062\u1063\u1065\u1066\u1067\u1068\u1069\u1070\u1071\u1072\u1073\u1074\u1075\u1076\u1077\u1078\u1079\u107A\u107B\u107C\u1085])/g, "$1$3$2");  //new
	   output =  output.replace( /\u1064/g, "\u1004\u103A\u1039");
	   output =  output.replace( /\u104E/g, "\u104E\u1004\u103A\u1038");
	   output =  output.replace( /\u1086/g, "\u103F");
	   output =  output.replace( /\u1060/g, '\u1039\u1000');
	   output =  output.replace( /\u1061/g, '\u1039\u1001');
	   output =  output.replace( /\u1062/g, '\u1039\u1002');
	   output =  output.replace( /\u1063/g, '\u1039\u1003');
	   output =  output.replace( /\u1065/g, '\u1039\u1005');
	   output =  output.replace( /[\u1066\u1067]/g, '\u1039\u1006');
	   output =  output.replace( /\u1068/g, '\u1039\u1007');
	   output =  output.replace( /\u1069/g, '\u1039\u1008');
	   output =  output.replace( /\u106C/g, '\u1039\u100B');
	   output =  output.replace( /\u1070/g, '\u1039\u100F');
	   output =  output.replace( /[\u1071\u1072]/g, '\u1039\u1010');
	   output =  output.replace( /[\u1073\u1074]/g, '\u1039\u1011');
	   output =  output.replace( /\u1075/g, '\u1039\u1012');
	   output =  output.replace( /\u1076/g, '\u1039\u1013');
	   output =  output.replace( /\u1077/g, '\u1039\u1014');
	   output =  output.replace( /\u1078/g, '\u1039\u1015');
	   output =  output.replace( /\u1079/g, '\u1039\u1016');
	   output =  output.replace( /\u107A/g, '\u1039\u1017');
	   output =  output.replace( /\u107B/g, '\u1039\u1018');
	   output =  output.replace( /\u107C/g, '\u1039\u1019');
	   output =  output.replace( /\u1085/g, '\u1039\u101C');
	   output =  output.replace( /\u106D/g, '\u1039\u100C');
	   output =  output.replace( /\u1091/g, '\u100F\u1039\u100D');
	   output =  output.replace( /\u1092/g, '\u100B\u1039\u100C');
	   output =  output.replace( /\u1097/g, '\u100B\u1039\u100B');
	   output =  output.replace( /\u106F/g, '\u100E\u1039\u100D');
	   output =  output.replace( /\u106E/g, '\u100D\u1039\u100D');
	   output =  output.replace( /(\u103C)([\u1000-\u1021])(\u1039[\u1000-\u1021])?/g, "$2$3$1");
	   output =  output.replace( /(\u103E)(\u103D)([\u103B\u103C])/g, "$3$2$1");
	   output =  output.replace( /(\u103E)([\u103B\u103C])/g, "$2$1");
	   output =  output.replace( /(\u103D)([\u103B\u103C])/g, "$2$1");
	   output = output.replace(/(([\u1000-\u101C\u101E-\u102A\u102C\u102E-\u103F\u104C-\u109F]))(\u1040)(?=\u0020)?/g, function($0, $1)
	   {
		  return $1 ? $1 + '\u101D' : $0 + $1;
	   }
	   );
	   output = output.replace(/((\u101D))(\u1040)(?=\u0020)?/g, function($0, $1)
	   {
		  return $1 ? $1 + '\u101D' : $0 + $1;
	   } );
	   output = output.replace(/(([\u1000-\u101C\u101E-\u102A\u102C\u102E-\u103F\u104C-\u109F\u0020]))(\u1047)/g, function($0, $1)
	   {
		  return $1 ? $1 + '\u101B' : $0 + $1;
	   });
	   output =  output.replace( /(\u1047)( ? = [\u1000 - \u101C\u101E - \u102A\u102C\u102E - \u103F\u104C - \u109F\u0020])/g, "\u101B");
	   output =  output.replace( /(\u1031)?([\u1000-\u1021])(\u1039[\u1000-\u1021])?([\u102D\u102E\u1032])?([\u1036\u1037\u1038]{0,2})([\u103B-\u103E]{0,3})([\u102F\u1030])?([\u1036\u1037\u1038]{0,2})([\u102D\u102E\u1032])?/g, "$2$3$6$1$4$9$7$5$8");
	   output = output.replace(ans+u, u+ans);
	   output =  output.replace( /(\u103A)(\u1037)/g, "$2$1");
	   output = output.replace(/\u1036\u102F/g,'\u102F\u1036');
	   return output;
	}//Z1_Uni
	
	function Uni_Z1(input)
	{
	   var output = input;
	   output = output.replace(/\u104E\u1004\u103A\u1038/g, '\u104E');
	   output = output.replace(/\u102B\u103A/g, '\u105A');
	   output = output.replace(/\u102D\u1036/g, '\u108E');
	   output = output.replace(/\u103F/g, '\u1086');
	   output = output.replace(/(\u102F[\u1036]?)\u1037/g, function($0, $1)
	   {
		  return $1 ? $1 + '\u1094' : $0 + $1;
	   }
	   );
	   output = output.replace(/(\u1030[\u1036]?)\u1037/g, function($0, $1)
	   {
		  return $1 ? $1 + '\u1094' : $0 + $1;
	   }
	   );
	   output = output.replace(/(\u1014[\u103A\u1032]?)\u1037/g, function($0, $1)
	   {
		  return $1 ? $1 + '\u1094' : $0 + $1;
	   }
	   );
	   output = output.replace(/(\u103B[\u1032\u1036]?)\u1037/g, function($0, $1)
	   {
		  return $1 ? $1 + '\u1095' : $0 + $1;
	   }
	   );
	   output = output.replace(/(\u103D[\u1032]?)\u1037/g,  function($0, $1)
	   {
		  return $1 ? $1 + '\u1095' : $0 + $1;
	   }
	   );
	   output = output.replace(/([\u103B\u103C\u103D][\u102D\u1036]?)\u102F/g, function($0, $1)
	   {
		  return $1 ? $1 + '\u1033' : $0 + $1;
	   }
	   );
	   output = output.replace(/((\u1039[\u1000-\u1021])[\u102D\u1036]?)\u102F/g,  function($0, $1)
	   {
		  return $1 ? $1 + '\u1033' : $0 + $1;
	   }
	   );
	   output = output.replace(/([\u100A\u100C\u1020\u1025\u1029][\u102D\u1036]?)\u102F/g, function($0, $1)
	   {
		  return $1 ? $1 + '\u1033' : $0 + $1;
	   }
	   );
	   output = output.replace(/([\u103B\u103C][\u103D]?[\u103E]?[\u102D\u1036]?)\u1030/g, function($0, $1)
	   {
		  return $1 ? $1 + '\u1034' : $0 + $1;
	   }
	   );
	   // uu - 2
	   output = output.replace(/((\u1039[\u1000-\u1021])[\u102D\u1036]?)\u1030/g, function($0, $1)
	   {
		  return $1 ? $1 + '\u1034' : $0 + $1;
	   }
	   );
	   // uu - 2
	   output = output.replace(/([\u100A\u100C\u1020\u1025\u1029][\u102D\u1036]?)\u1030/g, function($0, $1)
	   {
		  return $1 ? $1 + '\u1034' : $0 + $1;});
	   // uu - 2
	   output = output.replace(/(\u103C)\u103E/g, function($0, $1)
	   {
		  return $1 ? $1 + '\u1087' : $0 + $1;
	   });
	   // ha - 2
	   output = output.replace(/\u1009(?=[\u103A])/g, '\u1025');
	   output = output.replace(/\u1009(?=\u1039[\u1000-\u1021])/g, '\u1025');
	   // E render
	   output = output.replace( /([\u1000-\u1021\u1029])(\u1039[\u1000-\u1021])?([\u103B-\u103E\u1087]*)?\u1031/g, "\u1031$1$2$3");
		 // Ra render
	   output = output.replace( /([\u1000-\u1021\u1029])(\u1039[\u1000-\u1021\u1000-\u1021])?(\u103C)/g, "$3$1$2");
	   // Kinzi
	   output = output.replace(/\u1004\u103A\u1039/g, "\u1064");
	   // kinzi
	   output = output.replace(/(\u1064)([\u1031]?)([\u103C]?)([\u1000-\u1021])\u102D/g, "$2$3$4\u108B");
	   // reordering kinzi lgt
	   output = output.replace(/(\u1064)(\u1031)?(\u103C)?([ \u1000-\u1021])\u102E/g, "$2$3$4\u108C");
	   // reordering kinzi lgtsk
	   output = output.replace(/(\u1064)(\u1031)?(\u103C)?([ \u1000-\u1021])\u1036/g, "$2$3$4\u108D");
	   // reordering kinzi ttt
	   output = output.replace(/(\u1064)(\u1031)?(\u103C)?([ \u1000-\u1021])/g, "$2$3$4\u1064");
	   // reordering kinzi
	   // Consonant
	   output = output.replace(/\u100A(?=[\u1039\u102F\u1030])/g, "\u106B");
	   // nnya - 2
	   output = output.replace(/\u100A/g, "\u100A");
	   // nnya
	   output = output.replace(/\u101B(?=[\u102F\u1030])/g, "\u1090");
	   // ra - 2
	   output = output.replace(/\u101B/g, "\u101B");
	   // ra
	   output = output.replace(/\u1014(?=[\u1039\u103D\u103E\u102F\u1030])/g, "\u108F");
	   // na - 2
	   output = output.replace(/\u1014/g, "\u1014");
	   // na
	   // Stacked consonants
	   output = output.replace(/\u1039\u1000/g, "\u1060");
	   output = output.replace(/\u1039\u1001/g, "\u1061");
	   output = output.replace(/\u1039\u1002/g, "\u1062");
	   output = output.replace(/\u1039\u1003/g, "\u1063");
	   output = output.replace(/\u1039\u1005/g, "\u1065");
	   output = output.replace(/\u1039\u1006/g, "\u1066");
	   // 1067
	   output = output.replace(/([\u1001\u1002\u1004\u1005\u1007\u1012\u1013\u108F\u1015\u1016\u1017\u1019\u101D])\u1066/g, function($0, $1)
	   {
		  return $1 ? $1 + '\u1067' : $0 + $1;});
	   // 1067
	   output = output.replace(/\u1039\u1007/g, "\u1068");
	   output = output.replace(/\u1039\u1008/g, "\u1069");
	   output = output.replace(/\u1039\u100F/g, "\u1070");
	   output = output.replace(/\u1039\u1010/g, "\u1071");
	   // 1072 omit (little shift to right)
	   output = output.replace(/([\u1001\u1002\u1004\u1005\u1007\u1012\u1013\u108F\u1015\u1016\u1017\u1019\u101D])\u1071/g, function($0, $1)
	   {
		  return $1 ? $1 + '\u1072' : $0 + $1;});
	   // 1067
	   output = output.replace(/\u1039\u1011/g, "\u1073");
	   // \u1074 omit(little shift to right)
	   output = output.replace(/([\u1001\u1002\u1004\u1005\u1007\u1012\u1013\u108F\u1015\u1016\u1017\u1019\u101D])\u1073/g, function($0, $1)
	   {
		  return $1 ? $1 + '\u1074' : $0 + $1;});
	   // 1067
	   output = output.replace(/\u1039\u1012/g, "\u1075");
	   output = output.replace(/\u1039\u1013/g, "\u1076");
	   output = output.replace(/\u1039\u1014/g, "\u1077");
	   output = output.replace(/\u1039\u1015/g, "\u1078");
	   output = output.replace(/\u1039\u1016/g, "\u1079");
	   output = output.replace(/\u1039\u1017/g, "\u107A");
	   output = output.replace(/\u1039\u1018/g, "\u107B");
	   output = output.replace(/\u1039\u1019/g, "\u107C");
	   output = output.replace(/\u1039\u101C/g, "\u1085");
	   output = output.replace(/\u100F\u1039\u100D/g, "\u1091");
	   output = output.replace(/\u100B\u1039\u100C/g, "\u1092");
	   output = output.replace(/\u1039\u100C/g, "\u106D");
	   output = output.replace(/\u100B\u1039\u100B/g, "\u1097");
	   output = output.replace(/\u1039\u100B/g, "\u106C");
	   output = output.replace(/\u100E\u1039\u100D/g, "\u106F");
	   output = output.replace(/\u100D\u1039\u100D/g, "\u106E");
	   output = output.replace(/\u1009(?=\u103A)/g, "\u1025");
	   // u
	   output = output.replace(/\u1025(?=[\u1039\u102F\u1030])/g, "\u106A");
	   // u - 2
	   output = output.replace(/\u1025/g, "\u1025");
	   // u
	   /////////////////////////////////////
	   output = output.replace(/\u103A/g, "\u1039");
	   // asat
	   output = output.replace(/\u103B\u103D\u103E/g, "\u107D\u108A");
	   // ya wa ha
	   output = output.replace(/\u103D\u103E/g, "\u108A");
	   // wa ha
	   output = output.replace(/\u103B/g, "\u103A");
	   // ya
	   output = output.replace(/\u103C/g, "\u103B");
	   // ra
	   output = output.replace(/\u103D/g, "\u103C");
	   // wa
	   output = output.replace(/\u103E/g, "\u103D");
	   // ha
	   output = output.replace(/\u103A(?=[\u103C\u103D\u108A])/g, "\u107D");
	   // ya - 2
	   output = output.replace(/(\u100A(?:[\u102D\u102E\u1036\u108B\u108C\u108D\u108E])?)\u103D/g, function($0, $1)
	   {
		  //      return $1 ? $1 + '\u1087 ' : $0 + $1;
		  return $1 ? $1 + '\u1087' : $0 ;
	   });
	   // ha - 2
	   output = output.replace(/\u103B(?=[\u1000\u1003\u1006\u100F\u1010\u1011\u1018\u101A\u101C\u101E\u101F\u1021])/g, "\u107E");
	   // great Ra with wide consonants
	   output = output.replace(/\u107E([\u1000-\u1021\u108F])(?=[\u102D\u102E\u1036\u108B\u108C\u108D\u108E])/g, "\u1080$1");
	   // great Ra with upper sign
	   output = output.replace(/\u107E([\u1000-\u1021\u108F])(?=[\u103C\u108A])/g, "\u1082$1");
	   // great Ra with under signs
	   output = output.replace(/\u103B([\u1000-\u1021\u108F])(?=[\u102D \u102E \u1036 \u108B \u108C \u108D \u108E])/g, "\u107F$1");
	   // little Ra with upper sign
	   output = output.replace(/\u103B([\u1000-\u1021\u108F])(?=[\u103C\u108A])/g, "\u1081$1");
	   // little Ra with under signs
	   output = output.replace(/(\u1014[\u103A\u1032]?)\u1037/g, function($0, $1)
	   {
		  return $1 ? $1 + '\u1094' : $0 + $1;
	   }
	   );
	   // aukmyint
	   output = output.replace(/(\u1033[\u1036]?)\u1094/g, function($0, $1)
	   {
		  return $1 ? $1 + '\u1095' : $0 + $1;
	   }
	   );
	   // aukmyint
	   output = output.replace(/(\u1034[\u1036]?)\u1094/g, function($0, $1)
	   {
		  return $1 ? $1 + '\u1095' : $0 + $1;
	   }
	   );
	   // aukmyint
	   output = output.replace(/([\u103C\u103D\u108A][\u1032]?)\u1037/g, function($0, $1)
	   {
		  return $1 ? $1 + '\u1095' : $0 + $1;
	   }
	   );
	   
	   
	   // aukmyint
	   return output;
	}//Uni_Z1
	
	function flatMM(txt)
	{
		//remove zerowidth
		txt = txt.replace(/[\u200B]+/g,'');
		//split combined characters
		txt = txt.replace(/\u1088/g,'\u103D\u102F');
		txt = txt.replace(/\u1089/g,'\u103D\u1030');
		txt = txt.replace(/\u108B/g,'\u1064\u102D');
		txt = txt.replace(/\u108C/g,'\u1064\u102E');
		txt = txt.replace(/\u102F\u102D/g,'\u102D\u102F');
		txt = txt.replace(/\u102E\u102F/g,'\u102D\u102F');
		
		//variation to standard characters
		txt = txt.replace(/[\u107E-\u1084]/g,'\u103B');
		txt = txt.replace(/\u1033/g,'\u102F');
		txt = txt.replace(/\u1034/g,'\u1030');
		txt = txt.replace(/[\u1094\u1095]/g,'\u1037');
		txt = txt.replace(/\u107D/g,'\u103A');
		txt = txt.replace(/\u1087/g,'\u103D');
		txt = txt.replace(/႐/g,'ရ');
		
		txt = txt.replace(/\u1000/g,'\u1000');
		txt = txt.replace(/\u1000/g,'\u1000');
		txt = txt.replace(/\u1000/g,'\u1000');
		txt = txt.replace(/\u1000/g,'\u1000');
		txt = txt.replace(/\u1000/g,'\u1000');
		txt = txt.replace(/\u1000/g,'\u1000');
		txt = txt.replace(/\u1000/g,'\u1000');
		txt = txt.replace(/\u1000/g,'\u1000');

		//merge duplicated
		txt = mergeDup(txt);

		//fix error
		txt = txt.replace(/(ီု|ုီ)/g,'ို');
		txt = txt.replace(/(ီူ|ူီ|ိူ|ူိ)/g,'ို');
		txt = txt.replace(/(ိီ|ီိ)/g,'ီ');
		txt = txt.replace(/၄င္း/g,'၎');
		txt = txt.replace(/၎င္း/g,'၎');
		txt = txt.replace(/၇ွ/g,'ရွ');
		txt = txt.replace(/၇ိ/g,'ရိ');
		txt = txt.replace(/၇([ွ|ု|့|ိ|ံ|း|ာ|ီ|ဲ|ူ|ြ])/g,'ရ$1');
		txt = txt.replace(/ေ၇/g,'ေရ');
		txt = txt.replace(/(ညဏ္|ဥာဏ္)/g,'ဉာဏ္');
		txt = txt.replace(/င္\./g,'င့္');
		txt = txt.replace(/\u1000/g,'\u1000');
		txt = txt.replace(/\u1000/g,'\u1000');
		txt = txt.replace(/\u1000/g,'\u1000');
		txt = txt.replace(/\u1000/g,'\u1000');
		
		
		
		//re-arrange
		txt = reArrange(txt);
		txt = reArrange(txt);
		txt = reArrange(txt);
		txt = reArrange(txt);
		
		txt = txt.trim();
		return txt ;
	}
	
	function normalizeMM(txt)
	{	
		//remove zerowidth
		txt = txt.replace(/[\u200B]+/g,'');
		//merge duplicated
		txt = mergeDup(txt);
		//re-arrange
		txt = reArrange(txt);
		txt = reArrange(txt);
		txt = reArrange(txt);
		txt = reArrange(txt);
		
		txt = txt.replace(/\s့/g,'့');
		txt = txt.replace(/([^ည])\u103D\u102F/g,'$1\u1088');
		txt = txt.replace(/\u103D\u1030/g,'\u1089');
		txt = txt.replace(/်ြ/g,'ၽြ');
		txt = txt.replace(/ွ(ိ|ံ)ု/g,'$1ႈ');
		txt = txt.replace(/\s(္|့|ံ|႕|႔|း|ာ|ါ)/g,'$1');
		txt = txt.replace(/(ေ|ျ)\s/g,'$1');
		txt = txt.replace(/်([ိီံဲုဳူဴ])့/g,'်$1႕');
		txt = txt.replace(/ွံ့/g,'ွံ႔');
				
		txt = txt.replace(/(\u103B[\u1000-\u1021][\u1036\u102D\u102E\u1064\u108B\u108C\u1032\u103C])ု/g,'$1ဳ');
		txt = txt.replace(/([\u1000-\u1021]\u103A[\u1036\u102D\u102E\u1064\u108B\u108C\u1032\u103C]+)ု/g,'$1ဳ');
			
		txt = txt.replace(/\u103B([ကဃဆဏတထဘယလသဟအ])(ြိ|ိြ|ြီ|ီြ|ြဲ|ဲြ)/g,'\u1084$1$2');
		txt = txt.replace(/\u103B([ကဃဆဏတထဘယလသဟအ]([ံိီၤႋႌဲ]))/g,'\u1080$1$2');
		txt = txt.replace(/\u103B([ကဃဆဏတထဘယလသဟအ]([\u103C]))/g,'\u1082$1$2');
		txt = txt.replace(/\u103B([ကဃဆဏတထဘယလသဟအ])/g,'\u107E$1');
		
		
		txt = txt.replace(/([\u103Bၾ-ႄ])([က-အ])ွ/g,'$1$2\u1087');
		txt = txt.replace(/\u103B([က-အ])(ြိ|ိြ|ြီ|ီြ|ြဲ|ဲြ)/g,'\u1083$1$2');
		txt = txt.replace(/\u103B([က-အ])([်ႇ]?)([ံိီၤႋႌဲ])/g,'\u107F$1$2$3');
		txt = txt.replace(/\u103B([ကဃဆဏတထဘယလသဟအ]([\u103C]))/g,'\u1081$1$2');
		txt = txt.replace(/(ု့|့ု)/g,'ု႔');
		txt = txt.replace(/(ူ့|့ူ)/g,'ူ႔');
		txt = txt.replace(/(ဳ့|့ဳ)/g,'ဳ႕');
		txt = txt.replace(/(ြွ|ွြ)/g,'ႊ');
		txt = txt.replace(/ရ(ဲ့|့ဲ)/g,'ရဲ႕');
		txt = txt.replace(/ရ(ွ့|့ွ)/g,'ရွ႕');
		txt = txt.replace(/န(ဲ့|့ဲ)/g,'နဲ႔');
		txt = txt.replace(/န(ွ့|့ွ)/g,'နွ႔');
		txt = txt.replace(/န([္|ဲ|ိ|ု]?)့/g,'န$1႔');
		
		txt = txt.replace(/(ညိ|ည|ညံ|ညွ|ညွိ)ု/g,'$1ဳ');
		txt = txt.replace(/ညွ/g,'ညႇ');
		txt = txt.replace(/ရ့/g,'ရ႕');
		txt = txt.replace(/ရု/g,'႐ု');
		txt = txt.replace(/(ရို|ရိူ)/g,'႐ို');
		txt = txt.replace(/ရူ/g,'႐ူ');
		txt = txt.replace(/ရ(ိ|ံ)(ု|ူ|ႈ)/g,'႐$1$2');
		txt = txt.replace(/န([ြၵၱုူႊၷၶ\u1070-\u107C]|(ို|ံု))/g,'\u108F$1');
		txt = txt.replace(/နွ/g,'ႏွ');
		txt = txt.replace(/ြဲ့/g,'ြဲ႔');
		txt = txt.replace(/ြံ့/g,'ြံ႔');
		txt = txt.replace(/ြ့/g,'ြ႔');
		txt = txt.replace(/([ခဂငစဇဒဓႏပဖမဝဗ])\u1071/g,'$1\u1072');
		txt = txt.replace(/([ခဂငစဇဒဓႏပဖမဝဗ])\u1073/g,'$1\u1074');
		txt = txt.replace(/([ခဂငစဇဒဓႏပဖမဝဗ])\u107B/g,'$1\u1093');
		txt = txt.replace(/([ခဂငစဇဒဓႏပဖမဝဗ])\u1066/g,'$1\u1067');
		txt = txt.replace(/([ကဃဆဏတထဘယလသဟအ])\u1072/g,'$1\u1071');
		txt = txt.replace(/([ကဃဆဏတထဘယလသဟအ])\u1074/g,'$1\u1073');
		txt = txt.replace(/([ကဃဆဏတထဘယလသဟအ])\u1093/g,'$1\u107B');
		txt = txt.replace(/([ကဃဆဏတထဘယလသဟအ])\u1067/g,'$1\u1066');
		txt = txt.replace(/([ြ်\u1060-\u1063\u1065-\u1068\u106C\u106D\u1070-\u107D\u1085\u108A\u1093])(ိ|ီ|ံ|ဲ)?ု/g,'$1$2\u1033');
		txt = txt.replace(/([ြ်\u1060-\u1063\u1065-\u1068\u106C\u106D\u1070-\u107D\u1085\u108A\u1093])(ိ|ီ|ံ|ဲ)?့/g,'$1$2႔');
		
		txt = txt.replace(/([ျၾ-ႄ])([က-အ\.])ု/g,'$1$2ဳ');
		txt = txt.replace(/([ျၾ-ႄ])([က-အ\.])ူ/g,'$1$2ဴ');
		txt = txt.replace(/\u103A\u1030/g,'်ဴ');
		txt = txt.replace(/([\u1000-\u103D\u104A-\u1097\s])၀([\u1000-\u103D\u104A-\u1097\s])/g,'$1ဝ$2');
		txt = txt.replace(/([၀-၉])([\u1000-\u103D\u104A-\u1097\s])/g,'$1 $2');
		txt = txt.replace(/([\u1000-\u103D\u104A-\u1097\s])([၀-၉])/g,'$1 $2');
		txt = txt.replace(/(။|၊|\)|%|\u108F\u103D\u1004\u1039\u1037|၍|၏)([^\s])/g,'$1 $2');
		txt = txt.replace(/ရႈ/g,'႐ႈ');
		txt = txt.replace(/ဉ္/g,'ဥ္');
		
		
		txt = txt.replace(/\u1000/g,'\u1000');
		txt = txt.replace(/\u1000/g,'\u1000');
		txt = txt.replace(/\u1000/g,'\u1000');
		txt = txt.replace(/\u1000/g,'\u1000');
		txt = txt.replace(/ညႊ/g,'ၫႊ');
		txt = txt.replace(/ကို(^(င္)(း)(့)(႔)(႕)(တ္)(က္)(ယ္)(ကို)(ေရ)(လဲ)(လည္း))/g,'ကို $1');
		txt = txt.replace(/ညိႈ/g,'ညိႇဳ');
		
		//merge duplicated
		txt = mergeDup(txt);

		txt = txt.trim();
		return txt ;
	}

	function mergeDup(txt)
	{
		
		txt = txt.replace(/[\u102D]+/g,'\u102D');
		txt = txt.replace(/[\u102E]+/g,'\u102E');
		txt = txt.replace(/[\u1039]+/g,'\u1039');
		txt = txt.replace(/[\u102F]+/g,'\u102F');
		txt = txt.replace(/[\u1030]+/g,'\u1030');
		txt = txt.replace(/[\u1038]+/g,'\u1038');
		txt = txt.replace(/[\u102C]+/g,'\u102C');
		txt = txt.replace(/[\u1037]+/g,'\u1037');
		txt = txt.replace(/[\u103A]+/g,'\u103A');
		txt = txt.replace(/[\u103C]+/g,'\u103C');
		txt = txt.replace(/[\u103B]+/g,'\u103B');
		txt = txt.replace(/[\u1031]+/g,'\u1031');
		txt = txt.replace(/[\u1032]+/g,'\u1032');
		txt = txt.replace(/[\u1064]+/g,'\u1064');
		txt = txt.replace(/[\u108B]+/g,'\u108B');
		txt = txt.replace(/[\u108C]+/g,'\u108C');
		txt = txt.replace(/[\u108D]+/g,'\u108D');
		txt = txt.replace(/[\u108E]+/g,'\u108E');
		txt = txt.replace(/[\u1036]+/g,'\u1036');
		txt = txt.replace(/[\u0000]+/g,'\u0000');
		
		return txt ;
	}

	function reArrange(txt)
	{
		
		txt = txt.replace(/\u1037\u1039/g,'\u1039\u1037');
		txt = txt.replace(/ုိ/g,'ို');
		txt = txt.replace(/(ုိ့|ု့ိ|ိ့ု|ို့)/g,'\u102D\u102F\u1037');
		txt = txt.replace(/ိ်/g,'်ိ');
		txt = txt.replace(/(်ုိ|်ိူ|်ူိ)/g,'်ို');
		txt = txt.replace(/(်ိ့ု|်ုိ့|်ိ့ူ|်ူိ့|်ီ့ု|်ုီ့|်ီ့ူ|်ူီ့)/g,'်ို့');
		txt = txt.replace(/ီြ/g,'ြီ');
		txt = txt.replace(/ိြ/g,'ြိ');
		txt = txt.replace(/းု/g,'ုး');
		txt = txt.replace(/\းူ/g,'ူး');
		txt = txt.replace(/ွ်/g,'်ွ');
		txt = txt.replace(/်ွ/g,'ွ်');
		txt = txt.replace(/ီွ/g,'ွီ');
		txt = txt.replace(/ိွ/g,'ွိ');
		txt = txt.replace(/္ာ/g,'ာ္');
		txt = txt.replace(/့ာ/g,'ာ့');
		txt = txt.replace(/းာ/g,'ား');
		txt = txt.replace(/းီ/g,'ီး');
		txt = txt.replace(/့ိ/g,'ိ့');
		txt = txt.replace(/့ဲ/g,'ဲ့');
		txt = txt.replace(/ာွ/g,'ွာ');
		txt = txt.replace(/း္/g,'္း');
		txt = txt.replace(/်ံ့/g,'်ံ႕');
		txt = txt.replace(/း့/g,'့း');
		txt = txt.replace(/ုံ/g,'ံု');
		txt = txt.replace(/့ံ/g,'ံ့');
		txt = txt.replace(/\u1000/g,'\u1000');

		return txt ;	
	}

})();